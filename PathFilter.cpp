//
// PathFilter.ipp
// Copyright (c) Charles Baker. All rights reserved.
//

#include "stdafx.hpp"
#include "PathFilter.hpp"
#include "functions.hpp"
#include <sweet/assert/assert.hpp>

using namespace sweet::persist;

PathFilter::PathFilter( const filesystem::path& path )
: path_( path )
{
}

std::string PathFilter::to_memory( const std::string& value ) const
{
    filesystem::path path( widen(value) );
    if ( !path.empty() && !path.is_absolute() )
    {
        path = path_ / path;
    }
    return path.string();
}

std::wstring PathFilter::to_memory( const std::wstring& value ) const
{
    filesystem::path path( value );
    if ( !path.empty() && !path.is_absolute() )
    {
        path = path_ / path;
    }
    return path.wstring();
}

std::string PathFilter::to_archive( const std::string& value ) const
{
    SWEET_ASSERT( value.empty() || filesystem::path(value).is_absolute() );
    
    filesystem::path relative_path;
    if ( !value.empty() )
    {
        relative_path = relative( filesystem::path(value), path_ );
    }
    return relative_path.string();
}

std::wstring PathFilter::to_archive( const std::wstring& value ) const
{
    SWEET_ASSERT( value.empty() || filesystem::path(value).is_absolute() );
 
    filesystem::path relative_path;
    if ( !value.empty() )
    {
        relative_path = relative( filesystem::path(value), path_ );
    }
    return relative_path.wstring();
}

/**
// Express \e path as a path relative to \e base_path.
//
// If this \e base_path is empty or \e base_path and \e path are on different
// drives then no conversion is done and \e path is returned.
//
// @param path
//  The path to get a relative path to.
//
// @param base_path
//  The path to convert \e path to be relative from.
// 
// @return
//  The path \e path expressed relative to \e base_path.
*/
filesystem::path PathFilter::relative( const filesystem::path& path, const filesystem::path& base_path )
{
    // If the base path is empty or the path are on different drives then no 
    // conversion is done.
    if ( base_path.empty() || (base_path.has_root_name() && path.has_root_name() && base_path.root_name() != path.root_name()) )
    {
        return path;
    }

    // Find the first elements that are different in both of the paths.
    filesystem::path::const_iterator i = base_path.begin();
    filesystem::path::const_iterator j = path.begin();
    while ( i != base_path.end() && j != path.end() && *i == *j )
    {
        ++i;
        ++j;
    }
    
    // Add a leading parent element ("..") for each element that remains in the 
    // base path.
    const char* PARENT = "..";
    const char* SEPARATOR = "/";
    std::string relative_path;
    while ( i != base_path.end() )
    {
        relative_path.append( PARENT );
        relative_path.append( SEPARATOR );
        ++i;
    }

    // Add the remaining elements from the related path.
    if ( j != path.end() )
    {
        relative_path.append( j->generic_string() );
        ++j;
        while ( j != path.end() ) 
        {
            relative_path.append( SEPARATOR );
            relative_path.append( j->generic_string() );
            ++j;
        }
    }
    
    return filesystem::path( relative_path );
}

PathFilter sweet::persist::path_filter( const filesystem::path& path )
{
    return PathFilter( path );
}
