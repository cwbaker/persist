//
// LuaReader.cpp
// Copyright (c) 2008 - 2011 Charles Baker.  All rights reserved.
//

#include "stdafx.hpp"
#include "LuaReader.hpp"
#include "LuaParser.hpp"
#include "Reader.ipp"
#include "Writer.ipp"

using namespace sweet;
using namespace sweet::persist;

LuaReader::LuaReader( error::ErrorPolicy& error_policy )
: TextReader( error_policy ),
  error_policy_( error_policy )
{
}

LuaReader::LuaReader( const TextReader& reader )
: TextReader( reader ),
  error_policy_( reader.error_policy() )
{
}

void LuaReader::parse( const char* filename, Element* element )
{
    LuaParser parser( filename, element, &error_policy_ );
}

void LuaReader::parse( const wchar_t* filename, Element* element )
{
    LuaParser parser( filename, element, &error_policy_ );
}

void LuaReader::parse( std::istream& stream, Element* element )
{
    LuaParser parser( stream, element, &error_policy_ );
}
