#ifndef SWEET_PERSIST_FILTERS_HPP_INCLUDED
#define SWEET_PERSIST_FILTERS_HPP_INCLUDED

#include <sweet/build.hpp>
#include "declspec.hpp"
#include "Reference.hpp"
#include "Error.hpp"
#include "PathFilter.hpp"
#include "EnumFilter.hpp"
#include "MaskFilter.hpp"

#endif
