#ifndef SWEET_PERSIST_PATHFILTER_HPP_INCLUDED
#define SWEET_PERSIST_PATHFILTER_HPP_INCLUDED

#include <sweet/build.hpp>
#include "declspec.hpp"
#ifdef BUILD_OS_WINDOWS
#include <filesystem>
namespace filesystem = std::experimental::filesystem;
#else
#include <boost/filesystem.hpp>
namespace filesystem = boost::filesystem;
#endif

namespace sweet
{

namespace persist
{

/**
// @internal
//
// A simple filter that converts from absolute paths in memory to paths 
// relative to the archive path in an archive.
*/
class SWEET_PERSIST_DECLSPEC PathFilter
{
    const filesystem::path& path_; ///< The path to make paths relative to.

public:
    PathFilter( const filesystem::path& path );
    std::string to_memory( const std::string& value ) const;
    std::wstring to_memory( const std::wstring& value ) const;
    std::string to_archive( const std::string& value ) const;
    std::wstring to_archive( const std::wstring& value ) const;

private:
    static filesystem::path relative( const filesystem::path& path, const filesystem::path& base_path );
};

SWEET_PERSIST_DECLSPEC PathFilter path_filter( const filesystem::path& path );

}

}

#endif

